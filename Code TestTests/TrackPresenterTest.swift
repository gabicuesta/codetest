//
//  TrackPresenterTest.swift
//  Code Test
//
//  Created by Gabriel Cuesta Arza on 23/7/17.
//  Copyright © 2017 Gabriel Cuesta. All rights reserved.
//

import XCTest
@testable import Code_Test

class TrackServiceMock: TrackService {
    fileprivate let tracks: [Track]
    init(tracks: [Track]) {
        self.tracks = tracks
    }
    override func getProducts(searchTerm:String, _ callBack: @escaping ([Track]) -> Void) {
        callBack(tracks)
    }
    
}

class TrackViewMock : NSObject, TrackView{
    var setTracksCalled = false
    var setEmptyTracksCalled = false
    
    func setTracks(_ tracks: [TrackViewData]) {
        setTracksCalled = true
    }
    
    func setEmptyTracks() {
        setEmptyTracksCalled = true
    }
    
    func startLoading() {
    }
    
    func finishLoading() {
    }
    
}

class TrackPresenterTest: XCTestCase {
    
    let emptyTracksServiceMock = TrackServiceMock(tracks:[Track]())
    
    let towTracksServiceMock = TrackServiceMock(tracks:[Track(trackName: "trackName", artistName: "artistName", albumName: "albumName", thumbnail: "http://", price: 0.00, releaseDate: Date(), currency: "USD"),
                                                     Track(trackName: "trackName1", artistName: "artistName1", albumName: "albumName1", thumbnail: "http://", price: 1.00, releaseDate: Date(), currency: "USD")])
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShouldSetEmptyIfNoTrackAvailable() {
        //given
        let trackViewMock = TrackViewMock()
        let trackPresenterUnderTest = TrackPresenter(trackService: emptyTracksServiceMock)
        trackPresenterUnderTest.attachView(trackViewMock)
        
        //when
        trackPresenterUnderTest.getTracks(searchTerm: "")
        
        //verify
        XCTAssertTrue(trackViewMock.setEmptyTracksCalled)
    }
    
    func testShouldSetTracks() {
        //given
        let trackViewMock = TrackViewMock()
        let trackPresenterUnderTest = TrackPresenter(trackService: towTracksServiceMock)
        trackPresenterUnderTest.attachView(trackViewMock)
        
        //when
        trackPresenterUnderTest.getTracks(searchTerm: "U2")
        
        //verify
        XCTAssertTrue(trackViewMock.setTracksCalled)
    }
    
}
