//
//  TrackPresenter.swift
//  Code Test
//
//  Created by Gabriel Cuesta Arza on 21/7/17.
//  Copyright © 2017 Gabriel Cuesta. All rights reserved.
//

import Foundation

struct TrackViewData{
    
    let trackName: String
    let bandName: String
    let thumbnail: String
    let albumName: String
    let price: Double
    let releaseDate: Date
    let currency:String
}

protocol TrackView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setTracks(_ tracks: [TrackViewData])
    func setEmptyTracks()
}

class TrackPresenter {
    fileprivate let trackService:TrackService
    weak fileprivate var trackView : TrackView?
    
    init(trackService:TrackService){
        self.trackService = trackService
    }
    
    func attachView(_ view:TrackView){
        trackView = view
    }
    
    func detachView() {
        trackView = nil
    }
    
    func getTracks(searchTerm:String){
        self.trackView?.startLoading()
        
        trackService.getProducts(searchTerm: searchTerm){ [weak self] products in
            self?.trackView?.finishLoading()
            if(products.count == 0){
                self?.trackView?.setEmptyTracks()
            }else{
                let mappedTracks = products.map{
                    return TrackViewData(trackName: "\($0.trackName)", bandName: "\($0.artistName)", thumbnail: "\($0.thumbnail)", albumName: "\($0.albumName)", price:  ($0.price), releaseDate: ($0.releaseDate), currency: "\($0.currency)")
                }
                self?.trackView?.setTracks(mappedTracks)
            }
            
        }
        
    }
    
}
