import UIKit

class TrackViewController: UIViewController {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var emptyView: UIView?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?

    fileprivate let trackPresenter = TrackPresenter(trackService: TrackService())
    fileprivate var tracksToDisplay = [TrackViewData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = self
        
        searchField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        activityIndicator?.hidesWhenStopped = true

        trackPresenter.attachView(self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "list2Detail"){
            let nextSection = segue.destination as? DetailViewController
            nextSection?.trackInfo = sender as AnyObject
        }
    }
    
}

extension TrackViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracksToDisplay.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "TrackCell")
        let trackViewData = tracksToDisplay[indexPath.row]
        cell.textLabel?.text = trackViewData.trackName
        cell.detailTextLabel?.text = trackViewData.bandName
        
        cell.imageView?.image = UIImage(named: "placeholder")
        cell.imageView?.downloadedFrom(link: trackViewData.thumbnail)
        
        let pair = indexPath.row % 2
        if(pair==0){
            cell.backgroundColor = UIColor(red:0.89, green:0.95, blue:0.99, alpha:1.0)
        }else{
            cell.backgroundColor = UIColor(red:0.73, green:0.87, blue:0.98, alpha:1.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let trackViewData = tracksToDisplay[indexPath.row]
        
        var someDict = [String : AnyObject]()
        someDict["trackName"] = trackViewData.trackName as AnyObject
        someDict["bandName"] = trackViewData.bandName as AnyObject
        someDict["thumbnail"] = trackViewData.thumbnail as AnyObject
        someDict["albumName"] = trackViewData.albumName as AnyObject
        someDict["price"] = trackViewData.price as AnyObject
        someDict["date"] = trackViewData.releaseDate as AnyObject
        someDict["currency"] = trackViewData.currency as AnyObject
        
        self.performSegue(withIdentifier: "list2Detail", sender: someDict)
    }
    
}

extension TrackViewController: TrackView {

    func startLoading() {
        activityIndicator?.startAnimating()
    }

    func finishLoading() {
        activityIndicator?.stopAnimating()
    }

    func setTracks(_ tracks: [TrackViewData]) {
        tracksToDisplay = tracks
        tableView?.isHidden = false
        emptyView?.isHidden = true;
        tableView?.reloadData()
    }

    func setEmptyTracks() {
        tableView?.isHidden = true
        emptyView?.isHidden = false;
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        trackPresenter.getTracks(searchTerm: textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        trackPresenter.getTracks(searchTerm: textField.text!)
        return false
    }

}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
