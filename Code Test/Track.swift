//
//  Track.swift
//  Code Test
//
//  Created by Gabriel Cuesta Arza on 21/7/17.
//  Copyright © 2017 Gabriel Cuesta. All rights reserved.
//

import Foundation

struct Track {
    
    let trackName: String
    let artistName: String
    let albumName: String
    let thumbnail: String
    let price: Double
    let releaseDate: Date
    let currency:String
    
}
