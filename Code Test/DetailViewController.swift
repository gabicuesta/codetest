//
//  DetailView.swift
//  Code Test
//
//  Created by Gabriel Cuesta Arza on 22/7/17.
//  Copyright © 2017 Gabriel Cuesta. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var test:String?
    var trackInfo:AnyObject?
    
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var bandName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var launchDate: UILabel!
    @IBOutlet weak var picture: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let trackPrice = trackInfo?["price"] as! Double
        let currency = trackInfo?["currency"] as! String
        
        let date = trackInfo?["date"] as! Date
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        trackName.text  = trackInfo?["trackName"] as? String
        albumName.text  = trackInfo?["albumName"] as? String
        bandName.text   = trackInfo?["bandName"] as? String
        price.text      = "\(trackPrice) \(currency)"
        launchDate.text = dateFormatter.string(from: date)
        
        picture.layer.borderWidth = 1
        picture.layer.masksToBounds = false
        picture.layer.borderColor = UIColor.black.cgColor
        picture.layer.cornerRadius = picture.frame.size.height/2
        picture.clipsToBounds = true
        picture.downloadedFrom(link: trackInfo?["thumbnail"] as! String)
        
        
    }

}

