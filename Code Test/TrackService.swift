//
//  TrackService.swift
//  Code Test
//
//  Created by Gabriel Cuesta Arza on 21/7/17.
//  Copyright © 2017 Gabriel Cuesta. All rights reserved.
//

import Foundation

class TrackService {
    
    func getProducts(searchTerm:String,_ callBack:@escaping ([Track]) -> Void){
        
        var term = searchTerm
        
        term = term.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
        
        term = term.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!

        
        let urlString = "https://itunes.apple.com/search?term=" + term
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!, completionHandler: {(data, response, error) in
            guard let data = data, error == nil else { return }
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                let items = json["results"] as? [[String: Any]] ?? []
                
                var tracks = [Track]()
                
                var sTrackName:String
                var sArtistName:String
                var sAlbumName:String
                var sThumbnail:String
                var dTrackPrice:Double
                var sReleaseDate:String
                var dReleaseDate:Date
                var sCurrency:String
                
                for i in (0..<items.count){
                    if (items[i]["trackName"] as? String) != nil{
                        sTrackName = items[i]["trackName"] as! String
                    }else{
                        sTrackName = ""
                    }
                    
                    if (items[i]["artistName"] as? String) != nil{
                        sArtistName = items[i]["artistName"] as! String
                    }else{
                        sArtistName = ""
                    }
                    
                    if (items[i]["collectionName"] as? String) != nil{
                        sAlbumName  = (items[i]["collectionName"] as? String)!
                    }else{
                        sAlbumName  = ""
                    }
                    
                    if (items[i]["collectionName"] as? String) != nil{
                        sThumbnail = items[i]["artworkUrl100"] as! String
                    }else{
                        sThumbnail = ""
                    }
                    
                    if(items[i]["trackPrice"] as? Double) != nil{
                        dTrackPrice = items[i]["trackPrice"] as! Double
                    }else{
                        dTrackPrice = 0.00;
                    }
                    
                    sReleaseDate    = items[i]["releaseDate"] as! String
                    dReleaseDate    = self.string2Date(dateString: sReleaseDate)
                    
                    if(items[i]["currency"] as? String) != nil{
                        sCurrency = items[i]["currency"] as! String
                    }else{
                        sCurrency = "";
                    }
                    
                    tracks.append(Track(trackName: sTrackName, artistName: sArtistName, albumName: sAlbumName, thumbnail: sThumbnail, price: dTrackPrice, releaseDate: dReleaseDate, currency:sCurrency ))
                }
                
                DispatchQueue.main.async {
                    callBack(tracks)
                }
                
            } catch let error as NSError {
                print(error)
            }
            
            
        }).resume()
        
        
    }
    
    func string2Date(dateString: String) -> Date{
        
        let dateString = "1987-03-09T08:00:00Z"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        let dateObj = dateFormatter.date(from: dateString)
        
        return dateObj!
        
    }
}
