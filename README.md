# Code Test
*by Gabriel Cuesta Arza gabicuesta[at]gmail.com*

This __app__ it allows the user to search for his favorite artist or song on the __iTunes__ database.

App has a search engine, a result list and a detail view of every track.

To work it needs that your device has an active Internet connection.

I have used a MVP pattern design because I think that it fits well on the size of the project and allows me create good unit tests.

Project it is developed using __Xcode 8.3.3__ and __Swift 3.1__.

It works on __iPad__ and __iPhone__.

### Future improvements if I had more time:
- Streaming player to preview song
- Allow user to choose country of iTunes Store
- Allow user to share tracks on his social networks like Twitter, Facebook, etc.
- Create a local cache to store the downloaded files like pics
- Improve visual design
- Create a specific visual design for iPad

I hope that this demonstration fits your expectations :+1:
